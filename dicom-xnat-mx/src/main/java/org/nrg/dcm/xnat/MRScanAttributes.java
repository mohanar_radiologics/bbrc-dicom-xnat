/*
 * dicom-xnat-mx: org.nrg.dcm.xnat.MRScanAttributes
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;

import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;
import org.nrg.dcm.xnat.XnatAttrDef.Optional;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.FixedDicomAttributeIndex;
import org.dcm4che2.data.DicomObject;

import static org.nrg.dcm.DicomAttributes.*;

/**
 * mrScanData attributes
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 */
class MRScanAttributes {
    private MRScanAttributes() {} // no instantiation

    static public AttrDefs get() { return s; }

    static final private MutableAttrDefs s = new MutableAttrDefs(ImageScanAttributes.get());

    static {
        s.add(new VoxelResAttribute("parameters/voxelRes"));
        s.add(new OrientationAttribute("parameters/orientation"));
        s.add("coil", MR_RECEIVE_COIL_NAME);
        s.add(new MagneticFieldStrengthAttribute());
        s.add(new XnatAttrDef.Real("parameters/tr", MR_REPETITION_TIME));
        s.add(new MREchoTimeAttribute());
        s.add(Optional.wrap(new XnatAttrDef.Real("parameters/ti", MR_INVERSION_TIME)));
        s.add(new XnatAttrDef.Int("parameters/flip", MR_FLIP_ANGLE));
        s.add("parameters/sequence", Tag.SequenceName);
        s.add("parameters/imageType", Tag.ImageType);
        s.add("parameters/scanSequence", Tag.ScanningSequence);
        s.add("parameters/seqVariant", Tag.SequenceVariant);
        s.add("parameters/scanOptions", Tag.ScanOptions);
        s.add("parameters/acqType", Tag.MRAcquisitionType);
        s.add(new XnatAttrDef.Real("parameters/pixelBandwidth", MR_PIXEL_BANDWIDTH));
        s.add(new ImageFOVAttribute("parameters/fov"));

       
        
        DicomAttributeIndex GE_DTI_NUMBER_OF_DIRECTIONS = new FixedDicomAttributeIndex(0x001910e0) {
            public String getAttributeName(DicomObject o) {
                return "DTI Number of Directions";
            }
        };
     
        DicomAttributeIndex NUMBER_OF_TEMPORAL_POSITIONS = new FixedDicomAttributeIndex(0x00200105) {
            public String getAttributeName(DicomObject o) {
                return "number of temporal positions";
            }
        };
               
        s.add(XnatAttrDef.AddParam.wrap(GE_DTI_NUMBER_OF_DIRECTIONS));
        s.add(XnatAttrDef.AddParam.wrap(NUMBER_OF_TEMPORAL_POSITIONS));
        
      
        
        s.add("parameters/diffusion/bValues", MR_DIFF_B_VALUES);
        s.add("parameters/diffusion/directionality", MR_DIFF_DIRECTION);
        s.add("parameters/diffusion/orientations", MR_DIFF_ORIENTATION);
        s.add("parameters/diffusion/anisotropyType", MR_DIFF_ANISOTROPY_TYPE);
    }
}
